# eZ Platform AlloyEditor Requests Limit

This bundle is a temporary wrapper for [EZP-31410: Unable to edit an article with 100+ embed content/images](https://github.com/ezsystems/ezplatform-admin-ui/pull/1258) pull request.

## Installation

1. Add `ONLINE_EDITOR_REQUESTS_LIMIT` variable in `.env`:
    ```
    ...
    ###> contextualcode/ezplatform-alloyeditor-requests-limit ###
    ONLINE_EDITOR_REQUESTS_LIMIT=20
    ###< contextualcode/ezplatform-alloyeditor-requests-limit ###
    ```

2. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-requests-limit
    ```

3. Clear browser caches and enjoy!
