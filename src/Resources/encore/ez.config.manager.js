const path = require('path');
const richTextBase = './../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/';

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        itemToReplace: path.resolve(__dirname, richTextBase + 'js/OnlineEditor/plugins/ez-embed.js'),
        newItem: path.resolve(__dirname, '../public/js/alloyeditor/plugins/ez-embed.js')
    });
};