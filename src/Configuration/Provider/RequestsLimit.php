<?php

namespace ContextualCode\EzPlatformAlloyEditorRequestsLimit\Configuration\Provider;

use EzSystems\EzPlatformRichText\SPI\Configuration\Provider;

final class RequestsLimit implements Provider
{
    private $requestsLimit;

    public function __construct(int $requestsLimit) {
        $this->requestsLimit = $requestsLimit;
    }

    public function getName(): string
    {
        return 'alloyEditorExtensions';
    }

    public function getConfiguration(): array
    {
        return ['requestsLimit' => $this->requestsLimit];
    }
}
